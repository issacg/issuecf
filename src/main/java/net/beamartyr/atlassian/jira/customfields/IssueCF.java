package net.beamartyr.atlassian.jira.customfields;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import java.util.List;
import java.util.Map;

public class IssueCF extends AbstractSingleFieldType<Issue> {
    private static final Logger log = LoggerFactory.getLogger(IssueCF.class);

    public IssueCF(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
    super(customFieldValuePersister, genericConfigManager);
}
    
/*    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

        // This method is also called to get the default value, in
        // which case issue is null so we can't use it to add currencyLocale
        if (issue == null) {
            return map;
        }

         FieldConfig fieldConfig = field.getRelevantConfig(issue);
         //add what you need to the map here

        return map;
        
    }*/
    
    
	@Override
	public String getStringFromSingularObject(final Issue singularObject)
	{
	    if (singularObject == null)
	        return null;
	    else
	        return singularObject.getKey();
	}
	
	@Override
	public MutableIssue getSingularObjectFromString(final String string) 
	        throws FieldValidationException
	{
	    if (string == null)
	        return null;
	    try
	    {
	        final Issue issue = ComponentManager.getInstance().getIssueManager().getIssueObject(string);
	        // Check that we don't have too many decimal places
	        if (issue == null)
	        {
	            throw new FieldValidationException(
	                    "Target issue does not exist");
	        }
	        return (MutableIssue)issue;
	    }
	    catch (DataAccessException ex)
	    {
	        throw new FieldValidationException("Error accessing issue.");
	    }
	}
	

	@Override
	protected PersistenceFieldType getDatabaseType()
	{
	    return PersistenceFieldType.TYPE_LIMITED_TEXT;
	}

	@Override
	protected MutableIssue getObjectFromDbValue(final Object databaseValue)
	        throws FieldValidationException
	{
	    return getSingularObjectFromString((String) databaseValue);
	}
	

	@Override
	protected Object getDbValueFromObject(final Issue customFieldObject)
	{
	    return getStringFromSingularObject(customFieldObject);
	}
	
}