package net.beamartyr.atlassian.jira.customfields;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import net.beamartyr.atlassian.jira.customfields.IssueCF;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class IssueCFTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //IssueCF testClass = new IssueCF();

        throw new Exception("IssueCFTest has no tests!");

    }

}
